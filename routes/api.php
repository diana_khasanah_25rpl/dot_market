<?php

use Illuminate\Http\Request;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductCollection;
use App\Product;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
/**
*membuat routing API dengan prefix "localhost/api/v1/{bla bla}"
*/


Route::post('/register','API\V1\Auth\RegisterController@register');
Route::post('/login','API\V1\Auth\LoginController@login');
Route::group(['middleware' => 'auth:api'], function(){
	Route::get('/user/detail', 'API\V1\UserController@detail');

	Route::prefix('v1')->group(function(){
	Route::prefix('belajar')->group(function(){
		Route::get('cobaget','API\V1\CobaController@method_get');
		Route::post('cobapost','API\V1\CobaController@method_post');
		Route::post('cobatry', 'API\V1\CobaController@method_try');
	});
	Route::prefix('kategori')->group(function(){
		Route::get('kategori','API\V1\KategoriController@method_get');
		Route::get('kategori/{id}','API\V1\KategoriController@method_id');
		Route::post('kategori/add', 'API\V1\KategoriController@method_add');
	});
	Route::prefix('customers')->group(function(){
		Route::get('customers','API\V1\CustomersController@method_get');
		Route::get('customers/{id}','API\V1\CustomersController@method_id');
		Route::post('customers/add', 'API\V1\CustomersController@method_add');
	});

	Route::apiResource('category','API\V1\CategoryController');
	Route::apiResource('customer','API\V1\CustomerController');
	Route::apiResource('product', 'API\V1\ProductController');

	Route::post('order', 'API\V1\OrderController@store');
	Route::get('order/{id}', 'API\V1\OrderController@show');
	Route::post('order_detail', 'API\V1\OrderDetailController@store');
	Route::get('order_detail/{id}', 'API\V1\OrderDetailController@show');

	

	// Route::get('/product/{id}', function(Product $id){
	// 	return apiResponseBuilder(200, new ProductResource($id));
		
	// });

	// Route::get('/product', function(){
	// 	return apiResponseBuilder(200, ProductResource::collection(Product::all()));
	// });

	// Route::get('/products', function(){
	// 	return new ProductCollection(Product::all());
	// });

	});
});






