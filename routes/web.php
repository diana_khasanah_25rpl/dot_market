<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/**
*login routes
*/

Route::get('/login',"LoginController@showlogin")->name('login');
Route::post('/login',"LoginController@dologin")->name('login.doLogin');

/**
*logout routes
*/
Route::get('/logout',"LoginController@logout")->name('logout');
/**
*agar jika user yang tidak login tidak bisa melaukan apapun di dalamnya yakni middleeware
*/

Route::middleware("auth")->group(function(){

Route::get('/', function () {
    return view('template');
});
// Route::get('/product', function () {
//     return view('product');
// });
//percobaan route
//statis
Route::get('/kapal', function(){
	return "kapal ferry diana";
});
//dinamis
Route::get('/dermaga/{pulau}', function($nama){
	return "Dermaga menepi di $nama";
});

Route::get('/dermaga/{pulau}/{air}', function($nama,$air){
	return "Dermaga menepi pada $nama $air";
});

Route::get('/dermaga', 'CbController@kapal');
Route::get('/pelabuh/{labuh}', 'CbController@namakapal');

// Route::post('/kategori', 'KategoriController@submit');
// Route::get('/kategori/delete', 'KategoriController@delete');


	//kategory
	Route::prefix('categorys')->group(function(){
	Route::get('/','CategorysController@index');
	Route::get('/{id}','CategorysController@detail');
	Route::get('/{id}/edit','CategorysController@edit');
	Route::post('/{id}/update','CategorysController@update');
	Route::post('/store','CategorysController@store');
	Route::get('/{id}/delete','CategorysController@delete');
	});

	//customers
	Route::prefix('customers')->group(function(){
	Route::get('/','CustomersController@index');
	Route::get('/{id}','CustomersController@detail');
	Route::get('/{id}/edit','CustomersController@edit');
	Route::post('/{id}/update','CustomersController@update');
	Route::post('/store','CustomersController@store');
	Route::get('/{id}/delete','CustomersController@delete');
	});

	//orders
	Route::prefix('orders')->group(function(){
	Route::get('/', 'OrdersController@index');
	Route::post('/','OrdersController@store');
	Route::get('/detail/{id}', 'OrdersController@detail');
	Route::post('/detail/{id}/add', 'OrdersController@addOrder')
	   ->name('order.add');
	Route::get('/detail/{id}/delete', 'OrdersController@delete');
	});

	//product
	Route::prefix('product')->group(function(){
	Route::get('/','ProductController@index');
	Route::get('/{id}','ProductController@detail');
	Route::get('/{id}/edit','ProductController@edit');
	Route::post('/store','ProductController@store');
	Route::post('/{id}/update','ProductController@update');
	Route::get('/{id}/delete','ProductController@delete');
	});
	


});
