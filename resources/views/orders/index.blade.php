@include('base.header')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        halaman Orders
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Form Orders</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            @if ($message = Session::get('success'))
              <div class="alert alert-success">
                <p>{{ $message }}</p>
              </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div><br/>
            @endif
            <form role="form" action="/orders" method="post">
               @csrf
 
               <input type="hidden" class="form-control" name="order_id" id="order_id">
          
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nama Customers</label>
                  <!-- <input type="text" class="form-control" name="first_name" placeholder="Masukkan Nama Awal anda" required=""> -->
                  <select class="form-control" name="customer_id">
                      <option value="null">--Pilih Nama--</option>
                      @foreach($datacustomers as $item)
                      <option value="{{$item->id}}">{{$item->first_name}} {{$item->last_name}}</option>
                      @endforeach
                  </select>
                </div>
              </div> 

              
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Tambah</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Tabel Orders</h3>
              <form action="/orders" method="GET">
                <span class="pull-right">
                  <input type="text" name="search" class="form-control" placeholder="Search here ..">
                </span>
              </form>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th>No</th>
                  <th>Nama Customers</th>
                  <th>Total</th>
                  <th>Action</th>
                </tr>
                <?php $no = 1?>
            @foreach($dataOrders as $item)
                <tr>
                  <td>{{ $no }}</td>
                  <td>{{ $item->customers->first_name }} {{ $item->customers->last_name }}</td>
                  <td>{{ number_format($item->total, 2)}}</td>
                  <td>
                    <a class="btn btn-success" href="/orders/detail/{{$item->id}}">detail</a>
                   <!-- <a class="btn btn-danger" href="/orders/detail/{{$item->id}}/destroy">delete</a> -->
                   <a class="btn btn-danger" href="/orders/detail/{{$item->id}}/delete">delete</a>
                 </td>
                </tr>
                <?php $no++?>
            @endforeach
               

              </table>
               
            </div>
            <div class="text-center">
              {!! $dataOrders->appends(request()->all())->links() !!}
            </div>
            <div>
              
            </div>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('base.footer')
