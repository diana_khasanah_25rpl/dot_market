@include('base.header')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Halaman Categorys
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Tabel Categorys</h3>
            </div>
            <!-- /.box-header -->

            @if(Session::has('message'))
              <div class="callout callout-success">
               <h4><strong>{{session::get('message')}}</strong></h4>
               </div>
               @endif
            <form role="form" action="/categorys/{{$datacategorys->id}}/update" method="post">
               @csrf
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th>Id</th>
                  <th>:</th>
                   
                    <th> <input type="text" class="form-control" name="id" value="{{$datacategorys->id}}" readonly="true"></th>
                </tr>
                <tr>
                  <th>Nama Category</th>
                  <th>:</th>
                   
                  <th> <input type="text" class="form-control" name="name_category" value="{{$datacategorys->name_category}}" ></th>
                </tr>
                
                
              </table>
              <input  class="btn btn-primary" type="submit" value="update"></input>
              <a  class="btn btn-warning" href="/categorys">back</a>
            </div>
          </form>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('base.footer')
