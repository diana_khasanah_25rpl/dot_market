@include('base.header')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        halaman Category
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Form Category</h3>
            </div>
         
            <!-- /.box-header -->
            <!-- form start -->
            @if(Session::has('message'))
              <div class="callout callout-success">
               <h4><strong>{{session::get('message')}}</strong></h4>
               </div>
               @endif

                @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div><br/>
                @endif
            <form role="form" action="/categorys/store" method="post">
               @csrf

                <input type="hidden" class="form-control" name="id_category" id="id_category">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nama Category</label>
                  <input type="text" class="form-control" name="name_category" placeholder="Masukkan nama kategori">
                  
                </div>
              </div>
              
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Tambah</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Tabel Category</h3>
              <form action="/categorys" method="GET">
                <span class="pull-right">
                  <input type="text" name="search" class="form-control" placeholder="Search here ..">
                </span>
              </form>
            </div>
               
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th>No</th>
                  <th>Name Category</th>
                  <th>Jumlah Product</th>
                  
                  <th>Action</th>
                </tr>
                <?php $no=1?>
            @foreach($datacategorys as $item)
                <tr>
                  <td>{{ $no }}</td>
                  <td>{{ $item->name_category }}</td>
                  <td>{{ $item->product_count }}</td>
                  
                  <td>
                   <a class="btn btn-primary" href="categorys/{{$item->id}}/edit">edit</a>
                   <a class="btn btn-success" href="categorys/{{$item->id}}">detail</a>
                   <a class="btn btn-danger" href="categorys/{{$item->id}}/delete">delete</a>
                   
                </tr>
                <?php $no++?>
            @endforeach
               
              </table>
            </div>
             <div class="text-center">
              {!! $datacategorys->appends(request()->all())->links() !!}
            </div>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('base.footer')
