@include('base.header')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Halaman Category
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Tabel Category</h3>
            </div>
            <!-- /.box-header iki -->
            <div class="box-body">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>id produk</th>
                    <th>nama produk</th>
                    <thead></thead>
                  </tr>
                </thead>
                <body>
                  @forelse($datacategorys->products as $product)
                  <tr>
                    <td>{{$product->id}}</td>
                    <td>{{$product->name}}</td>
                  </tr>
                  @empty
                  <tr>
                    <td colspan="2" align="center">
                      Belum ada data produk dalam kategori ini
                    </td>
                  </tr>
               
                
                @endforelse
                 </body>
                
               
              </table>
              <a  class="btn btn-warning" href="/categorys">back</a>
            </div>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('base.footer')
