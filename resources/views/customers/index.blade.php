@include('base.header')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        halaman Customers
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Form Customers</h3>
            </div>
            
            <!-- /.box-header -->
            <!-- form start -->
            @if(Session::has('message'))
              <div class="callout callout-success">
               <h4><strong>{{session::get('message')}}</strong></h4>
               </div>
               @endif

                @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div><br/>
                @endif
            <form role="form" action="/customers/store" method="post">
               @csrf

               
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Email</label>
                  <input type="text" class="form-control" name="email" placeholder="Masukkan email anda">
                  
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">First Name</label>
                  <input type="text" class="form-control" name="first_name" placeholder="Masukkan Nama Awal anda">
                
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Last Name</label>
                  <input type="text" class="form-control" name="last_name" placeholder="Masukkan Nama Akhir anda">
                
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Address</label>
                  <input type="text" class="form-control" name="address" placeholder="Masukkan Alamat anda">
                
                </div>
              </div>
               <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Phone Number</label>
                  <input type="number" class="form-control" name="phone_number" placeholder="Masukkan Nomor Telepon anda" >
                
                </div>
              </div>
               <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Password</label>
                  <input type="text" class="form-control" name="password" placeholder="Masukkan Password anda">
                
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Tambah</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Tabel Customers</h3>

               <form action="/customers" method="GET">
                <span class="pull-right">
                  <input type="text" name="search" class="form-control" placeholder="Search here ..">
                </span>
              </form>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th>No</th>
                  <th>Email</th>
                   <th>First Name</th>
                  <th>Last Name</th>
                  <th>Address</th>
                  <th>Phone Number</th>
                  <th>Password</th>
                  <th>Action</th>
                </tr>
                <?php $no=1?>
            @foreach($datacustomers as $item)
                <tr>
                  <td>{{ $no }}</td>
                  <td>{{ $item->email }}</td>
                  <td>{{ $item->first_name }}</td>
                  <td>{{ $item->last_name }}</td>
                  <td>{{ $item->address}}</td>
                  <td>{{ $item->phone_number }}</td>
                  <td>{{ $item->password}}</td>
                  <td>
                   <a class="btn btn-primary" href="/customers/{{$item->id}}/edit">edit</a>
                   <a class="btn btn-success" href="/customers/{{$item->id}}">detail</a>
                   <a class="btn btn-danger" href="/customers/{{$item->id}}/delete">delete</a>
                   
                 </td>
                </tr>
                <?php $no++?>
            @endforeach
               
              </table>
            </div>
            <div class="text-center">
              {!! $datacustomers->appends(request()->all())->links() !!}
            </div>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('base.footer')
