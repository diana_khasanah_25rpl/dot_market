@include('base.header')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Halaman Customers
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Tabel Customers</h3>
            </div>
            <!-- /.box-header -->

            @if(Session::has('message'))
              <div class="callout callout-success">
               <h4><strong>{{session::get('message')}}</strong></h4>
               </div>
               @endif
            <form role="form" action="/customers/{{$datacustomers->id}}/update" method="post">
               @csrf
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th>Id</th>
                  <th>:</th>
                   
                    <th> <input type="text" class="form-control" name="id" value="{{$datacustomers->id}}" readonly="true"></th>
                </tr>
                <tr>
                  <th>Email</th>
                  <th>:</th>
                   
                    <th> <input type="text" class="form-control" name="email" value="{{$datacustomers->email}}" ></th>
                </tr>
                
                <tr>
                  <th>First Name</th>
                  <th>:</th>
                   
                    <th> <input type="text" class="form-control" name="first_name" value="{{$datacustomers->first_name}}"></th>
                </tr>
                <tr>
                  <th>Last Name</th>
                  <th>:</th>
                   <th> <input type="text" class="form-control" name="last_name" value="{{$datacustomers->last_name}}"></th>
                </tr>
                <tr>
                  <th>Address</th>
                  <th>:</th>
                   <th> <input type="text" class="form-control" name="address" value="{{$datacustomers->address}}"></th>
                </tr>
                <tr>
                  <th>Phone Number</th>
                  <th>:</th>
                   <th> <input type="number" class="form-control" name="phone_number" value="{{$datacustomers->phone_number}}"></th>
                </tr>
                <tr>
                  <th>Password</th>
                  <th>:</th>
                   <th> <input type="text" class="form-control" name="password" value="{{$datacustomers->password}}"></th>
                </tr>
              </table>
              <input  class="btn btn-primary" type="submit" value="update"></input>
              <a  class="btn btn-warning" href="/customers">back</a>
            </div>
          </form>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('base.footer')
