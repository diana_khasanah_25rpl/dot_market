@include('base.header')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Halaman Customers
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Tabel Customers</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th>Id</th>
                  <th>:</th>
                   <th>{{$datacustomers->id}}</th>
                </tr>
                <tr>
                  <th>Email</th>
                  <th>:</th>
                   <th>{{$datacustomers->email}}</th>
                </tr>
                
                <tr>
                  <th>First Name</th>
                  <th>:</th>
                   <th>{{$datacustomers->first_name}}</th>
                </tr>
                <tr>
                  <th>Last Name</th>
                  <th>:</th>
                   <th>{{$datacustomers->last_name}}</th>
                </tr>
                <tr>
                  <th>Address</th>
                  <th>:</th>
                   <th>{{$datacustomers->address}}</th>
                </tr>
                <tr>
                  <th>Phone Number</th>
                  <th>:</th>
                   <th>{{$datacustomers->phone_number}}</th>
                </tr>
                <tr>
                  <th>Password</th>
                  <th>:</th>
                   <th>{{$datacustomers->password}}</th>
                </tr>
                
                
               
              </table>
              <a  class="btn btn-warning" href="/customers">back</a>
            </div>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('base.footer')
