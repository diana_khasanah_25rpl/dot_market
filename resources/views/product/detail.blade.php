@include('base.header')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Halaman Product
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Tabel Product</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th>Id</th>                 
                   <th>{{$dataproduct->id}}</th>
                </tr>
                <tr>
                  <th>Nama</th>
                   <th>{{$dataproduct->name}}</th>
                </tr>
                </tr><tr>
                  <th>Kategori</th>                 
                   <th>{{$dataproduct->category->name_category}}</th>
                </tr>
                <tr>
                  <th>Foto Product</th>
                   <td><img src="/images/{{ $dataproduct->image }}" style="width: 50px; height: 40px"></td>
                </tr>
                <tr>
                  <th>Harga</th>
                  
                   <th>{{$dataproduct->unit_price}}</th>
                </tr>
               
              </table>
              <a  class="btn btn-warning" href="/product">back</a>
            </div>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('base.footer')
