@include('base.header')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Halaman Product
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Contoh Tabel</h3>
            </div>
            <!-- /.box-header -->
              @if(Session::has('message'))
               <h4><strong>{{session::get('message')}}</strong></h4>
               @endif
            <form role="form" action="/product/{{$dataproduct->id}}/update" method="post" enctype="multipart/form-data">
               @csrf

              
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th>Id</th>
                  
                   <th> <input type="text" class="form-control" name="id" value="{{$dataproduct->id}}" readonly="true"></th>
                </tr>
                <tr>
                  <th>Name Product</th>
                  
                   <th><input type="text" class="form-control" name="name" value="{{$dataproduct->name}}"></th>
                </tr>
                <tr>
                  <th>Category</th>
                  
                   <th>
                    <select class="form-control" name="category_id">
                      <option value="null">--Pilih Nama--</option>
                      @foreach($datacategorys as $item)
                      <option value="{{$item->id}}" {{($dataproduct->category_id == $item->id)?'selected':''}}>{{$item->name_category}}</option>
                      @endforeach
                  </select>
                </tr>
                <tr>
                  <th>Foto Product</th>
                  
                 
                       <td><img src="/images/{{ $dataproduct->image }}" style="width: 50px; height: 40px">
                       <input type="file" name="image"></td>
                    
                </tr>
                <tr>
                  <th>Harga</th>
                  
                   <th><input type="text" class="form-control" name="unit_price" value="{{$dataproduct->unit_price}}"></th>
                </tr>
               
              </table>
              <input  class="btn btn-primary" type="submit" value="update"></input>
              <a  class="btn btn-warning" href="/product">back</a>
            </div>
          </form>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('base.footer')
