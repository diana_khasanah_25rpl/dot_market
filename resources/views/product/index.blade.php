@include('base.header')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Halaman Product
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Form Product</h3>
            </div>

            <!-- /.box-header -->
            <!-- form start -->
               @if(Session::has('message'))
               <div class="callout callout-success">
               <h4><strong>{{session::get('message')}}</strong></h4>
               </div>
               @endif
                @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div><br/>
                @endif
            <form role="form" action="/product/store" method="post" enctype="multipart/form-data">
               @csrf

               
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Name Product</label>
                  <input type="text" class="form-control" name="name" placeholder="Masukkan nama produk anda">
                  
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Category</label>

                  <select class="form-control" name="category_id">
                      <option value="">--Pilih Category--</option>
                  @foreach($datacategorys as $item)

                    <option value="{{$item->id}}">{{$item->name_category}}</option>
                  @endforeach
                  <!-- <input type="text" class="form-control" name="category" placeholder="Masukkan harga" required=""> -->
                </select>
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Harga</label>
                  <input type="number" class="form-control" name="unit_price" placeholder="Masukkan unit price" >
                 
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Foto Product</label>
                  <input type="file" name="image" placeholder="Tambahkan foto Product">
                 
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Tambah</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Tabel Product</h3>
               <form action="/product" method="GET">
                <span class="pull-right">
                  <input type="text" name="search" class="form-control" placeholder="Search here ..">
                </span>
              </form>
               </div>
            <!-- /.box-search -->
              

          
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th>No</th>
                  <th>Name Product</th>
                   <th>Category</th>
                  <th>Harga</th>
                  <th>Foto Product</th>
                  <th>Action</th>

                </tr>

                <?php $no = 1 ?>
            @foreach($dataproduct as $item)
                <tr>
                 <td>{{ $no }}</td>
                  <td>{{ $item->name }}</td>
                  <td>{{ $item->category->name_category}}</td>
                  <td>{{ $item->unit_price }}</td>
                  <td><img src="/images/{{ $item->image }}" style="width: 50px; height: 40px"></td>
                  <td>
                    <a class="btn btn-primary" href="/product/{{$item->id}}/edit">edit</a>
                    <a class="btn btn-success" href="/product/{{$item->id}}">detail</a>
                    <a class="btn btn-danger" href="/product/{{$item->id}}/delete">delete</a>
                  </td>
                </tr>
                 <?php $no++ ?>
            @endforeach
               
              </table>
            </div>
             <div class="text-center">
              {!! $dataproduct->appends(request()->all())->links() !!}
            </div>

          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('base.footer')
  <!-- /.lak @ fuction atau metod
                  kalau field inptan
                  td awal key td kedua value
                -->
<!--  {{ csrf_field() }}  -->
             <!-- <div class="box-body">
                <div class="form-group">
                  <select class="form-control" name="nama_kategori" id="nama_kategori">
                   
                        <option>--Pilih Kategori Buku--</option>
                      <option value="topi">topi</option>
                  <option value="celana">celana</option>
                  <option value="sepatu">sepatu</option>
                     
                    
                  </select>
                </div>
              </div> -->