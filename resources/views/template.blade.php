@include('base.header')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Contoh Form dan Tabel
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Contoh Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
             <!-- @csrf-->
            <form role="form" action="/kategori" method="post">
             <!--  {{ csrf_field() }}  -->
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nama Kategori</label>
                  <input type="text" class="form-control" name="kategori" placeholder="Masukkan nama kategori produk">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Tambah</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Contoh Tabel</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th>No</th>
                  <th>Nama Kategori</th>
                </tr>
                <!-- /.lak @ fuction atau metod
                  lak field inptan
                  td awal key td kedua value
                -->

                @if(Session::get('kategori'))
                @foreach(Session::get('kategori') as $key => $value)
                <tr>
                  <td>{{ $key }}</td>
                  <td>{{ $value }}</td>
                  <td><button class="btn btn-danger" onclick="edit({{$value}})">edit</button></td>
                  <td><button class="btn btn-danger" onclick="edit({{$value}})">delete</button></td>

                </tr>

                @endforeach
                @endif
              </table>
            </div>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('base.footer')
<!-- 
  -->