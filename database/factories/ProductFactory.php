<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Product;
use App\Categorys;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        //
    	

        
        'name' => $faker->word,
        'unit_price' => $faker->randomDigit(),
        'image' => $faker->imageUrl($width = 640, $height = 480)
       

       
    ];

});

