<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Categorys;
use Faker\Generator as Faker;

$factory->define(Categorys::class, function (Faker $faker) {
    return [
        //
        'name_category'=> $faker->word,
        'product_count'=> $faker->randomDigit()
    ];
});
