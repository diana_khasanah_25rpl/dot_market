<?php

use Illuminate\Database\Seeder;
use App\Categorys;
class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //Categorys::create(['name_category' => "Belanja"]);
        factory(Categorys::class,2)->create();
    }
}
