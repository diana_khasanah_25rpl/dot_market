<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        return [
            'name' => $this->name,
            'harga' => $this->unit_price,
            'gambar' => asset('/images/'.$this->image),
            'id' => $this->id,
            'kategori' => [
                    'id' => $this->category_id,
                    'nama' => $this->category->name_category
                ]
        ];



        
       
    }
}
