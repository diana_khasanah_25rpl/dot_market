<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categorys;

class KategoriController extends Controller {

	public function method_get()
	{
		try {
			$data = Categorys::all();

			$code = 200;
			$response = $data; 

		} catch (Exception $e) {
			$code = 500;
			$response = $e->getMessage(); 
		}

		
		return apiResponseBuilder($code,$response);

	}

	public function method_id(Request $request,$id)
	{
		try {
			$data = Categorys::find($id);
			$code = 200;
			$response = $data;
			
		} catch (Exception $e) {
			$code = 500;
			$response = $e->getMessage(); 
		}
		
		return apiResponseBuilder($code,$response);

	}

	public function method_add(Request $request)
	{
		try {
			$data = new Categorys;
			if (!$data) throw new \Exception("Error Processing Request", 1);
			
			
			$data->name_category = $request->name_category;
			$data->product_count = 0;
			$data->save();

			$code = 200;
			$response = $data; 
			
		} catch (Exception $e) {
			$code = 500;
			$response = $e->getMessage(); 
			
		}
		return apiResponseBuilder($code,$response);
	}



}
?>