<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customers;
use App\Orders;
use App\OrderDetails;
use App\Product;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;

class OrderDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $this->validate($request, [
                'order_id' => 'required',
                'product_id' => 'required',
                'quantity' => 'required',

            ]);

             //get data order by id produk
        $checkProduct = OrderDetails::where('product_id', $request->product_id)
                                    ->where('order_id', $request->order_id)->first();
        //ambil data product by id untuk melihat data harga product
        $dataProduct = Product::where('id', $request->product_id)->first();

        if ($checkProduct) {
            $checkProduct->order_id = $checkProduct->order_id;
            $checkProduct->quantity = $checkProduct->quantity + $checkProduct->quantity;
            $checkProduct->price = $checkProduct->price +($request->quantity * $dataProduct->unit_price);
            $checkProduct->save();
        }else{//kondisi belum ada produk membuat detail order
            $dataOrderDetail = new OrderDetails;
            $dataOrderDetail->order_id = $request->order_id;
            $dataOrderDetail->product_id = $request->product_id;
            $dataOrderDetail->quantity = $request->quantity;
            $dataOrderDetail->price = $request->quantity * $dataProduct->unit_price;
            $dataOrderDetail->save();
        }

        $getOrder = OrderDetails::where('order_id',$request->order_id)->get();
        $total = $getOrder->sum('price');
        $data = Orders::find($request->order_id);
        $data->total = $total;
        $data->save();


        $code = 200;
        $response = $data;
            
        } catch (Exception $e) {
             if ($e instanceof ValidationException) {
               $code = 400;
               $response = $e->errors();
            }else{
                $code = 500;
                $response = $e->getMessage(); 
            }
        }
        return apiResponseBuilder($code,$response);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {

            $data = OrderDetails::findOrFail($id);
            $code = 200;
            $response = $data;
            
        } catch (Exception $e) {
            
            if ($e instanceof ModelNotFoundException) {
                $code = 404;
                $response = "not found data";
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
        }
        return apiResponseBuilder($code,$response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
