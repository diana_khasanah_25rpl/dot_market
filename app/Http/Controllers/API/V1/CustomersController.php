<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customers;

class CustomersController extends Controller {

	public function method_get()
	{
		try {
			$data = Customers::all();
			//$data = Customers::paginate(2);

			$code = 200;
			$response = $data; 

		} catch (Exception $e) {
			$code = 500;
			$response = $e->getMessage(); 
		}

		
		return apiResponseBuilder($code,$response);
	}

	public function method_id(Request $request,$id)
	{
		try {
			$data = Customers::find($id);
			$code = 200;
			$response = $data;
			
		} catch (Exception $e) {
			$data = 500;
			$response = $e->getMessage();
			
		}
		return apiResponseBuilder($code,$response);
	}

	public function method_add(Request $request)
	{
		try {
			$data = new Customers;
			if (!$data) throw new \Exception("Error Processing Request", 1);
			
			$data->email = $request->email;
			$data->first_name = $request->first_name;
			$data->last_name = $request->last_name;
			$data->address = $request->address;
			$data->phone_number = $request->phone_number;
			$data->password = $request->password;

			$data->save();

			$code = 200;
			$response = $data;
			
		} catch (Exception $e) {
			$code = 500;
			$response = $e->getMessage();
		}
		return apiResponseBuilder($code,$response);
	}


}
?>