<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customers;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            $data = Customers::paginate(3);
            $code = 200;
            $response = $data; 

        } catch (Exception $e) {

            $code = 500;
            $response = $e->getMessage(); 
        }

        
        return apiResponseBuilder($code,$response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $this->validate($request,[
         'email'          => 'required',
         'first_name'      => 'required',
         'last_name'    => 'required',
         'address' => 'required',
         'phone_number' => 'required|numeric',
         'password'=>"required",

         ]);

            $data = new Customers;
            if (!$data) throw new \Exception("Error Processing Request", 1);
            if (!isset($request->email)) throw new \Exception("data harus isi", 1);

            $data->email = $request->email;
            $data->first_name = $request->first_name;
            $data->last_name = $request->last_name;
            $data->address = $request->address;
            $data->phone_number = $request->phone_number;
            $data->password = $request->password;
            $data->save();

            $code = 200;
            $response = $data; 
            
        } catch (Exception $e) {
            // $code = 500;
            // $response = $e->getMessage(); 
            
            if ($e instanceof ValidationException) {
               $code = 400;
               $response = $e->errors();
            }else{
                $code = 500;
                $response = $e->getMessage(); 
            }
            
        }
        return apiResponseBuilder($code,$response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $data = Customers::findOrFail($id);
            $code = 200;
            $response = $data;
            
        } catch (Exception $e) {
            // $code = 500;
            // $response = $e->getMessage(); 
            if ($e instanceof ModelNotFoundException) {
               $code = 404;
               $response = 'not found data';
            }else{
                $code = 500;
                $response = $e->getMessage(); 
            }
        }
        
        return apiResponseBuilder($code,$response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {
            $this->validate($request,[
         'email'          => 'required',
         'first_name'      => 'required',
         'last_name'    => 'required',
         'address' => 'required',
         'phone_number' => 'required|numeric',
         'password'=>"required",

         ]);
            $data = Customers::find($id);

            if (!$data) throw new \Exception("Error Processing Request", 1);
            if (!isset($request->email)) throw new \Exception("data harus isi", 1);
            
            $data->email = $request->email;
            $data->first_name = $request->first_name;
            $data->last_name = $request->last_name;
            $data->address = $request->address;
            $data->phone_number = $request->phone_number;
            $data->password = $request->password;
            $data->save();

            $code = 200;
            $response = $data;
            
        } catch (Exception $e) {
            // $code = 500;
            // $response = $e->getMessage();
            if ($e instanceof ValidationException) {
               $code = 400;
               $response = $e->errors();
            }else{
                $code = 500;
                $response = $e->getMessage(); 
            } 
        }
        return apiResponseBuilder($code,$response);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
             $data = Customers::find($id);
             $data->delete();
             $code = 200;
            $response = $data;
            
        } catch (Exception $e) {
            $code = 500;
            $response = $e->getMessage(); 
        }
        return apiResponseBuilder($code,$response);
    }
}
