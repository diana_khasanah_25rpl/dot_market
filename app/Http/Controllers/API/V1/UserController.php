<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;

class UserController extends Controller
{
    //

    public function detail()
    {

    	try{
			$data= Auth::user();
            $code = 200;
            $response = $data; 
            
        } catch (Exception $e) {
            // $code = 500;
            // $response = $e->getMessage(); 
            
            if ($e instanceof ValidationException) {
               $code = 400;
               $response = $e->errors();
            }else{
                $code = 500;
                $response = $e->getMessage(); 
            }
            
        }
        return apiResponseBuilder($code,$response);
    	// $user = Auth::user();
    	// return apiResponseSuccess('Detail user login', $user, 200);
    }
}
