<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductCollection;
use App\Product;
use Session;
use App\Categorys;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            $data = ProductResource::collection(Product::paginate(3));
            $code = 200;
            $response = $data; 

        } catch (Exception $e) {

            $code = 500;
            $response = $e->getMessage(); 
        }
        return apiResponseBuilder($code,$response);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
           
        $this->validate($request,[
         'name'          => 'required|string',
         'category_id'      => 'required|string',
         'unit_price'    => 'required|numeric',
         'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

         ]);

            $data = new Product();
            if (!$data) throw new \Exception("Error Processing Request", 1);
            if (!isset($request->name)) throw new \Exception("data harus isi", 1);
            Categorys::where('id',$request->category_id)->increment('product_count');
        
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'),$imageName);
            
            $data->name = $request->name;
            $data->category_id = $request->category_id;
            $data->unit_price = $request->unit_price;
            $data->image = $imageName;
            $data->save();

            $code = 200;
            $response = new ProductResource($data); 
            
        } catch (Exception $e) {
            // $code = 500;
            // $response = $e->getMessage(); 
            
            if ($e instanceof ValidationException) {
               $code = 400;
               $response = $e->errors();
            }else{
                $code = 500;
                $response = $e->getMessage(); 
            }
            
        }
        return apiResponseBuilder($code,$response);    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $data = new ProductResource(Product::findOrFail($id));
            $code = 200;
            $response = $data;
            
        } catch (Exception $e) {
            // $code = 500;
            // $response = $e->getMessage(); 
            if ($e instanceof ModelNotFoundException) {
               $code = 404;
               $response = 'not found data';
            }else{
                $code = 500;
                $response = $e->getMessage(); 
            }
        }
        
        return apiResponseBuilder($code,$response);    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {
            $this->validate($request,[
         'name'          => 'required|string',
         'category_id'      => 'required|string',
         'unit_price'    => 'required|numeric',
         'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

         ]);
            $data = Product::find($id);

            if (!$data) throw new \Exception("Error Processing Request", 1);
            if (!isset($request->name)) throw new \Exception("data harus isi", 1);
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'),$imageName);
            
            $data->name = $request->name;
            $data->category_id = $request->category_id;
            $data->unit_price = $request->unit_price;
            $data->image = $imageName;
            $data->save();

            $code = 200;
            $response = new ProductResource($data); 
            
        } catch (Exception $e) {
            // $code = 500;
            // $response = $e->getMessage();
            if ($e instanceof ValidationException) {
               $code = 400;
               $response = $e->errors();
            }else{
                $code = 500;
                $response = $e->getMessage(); 
            } 
        }
        return apiResponseBuilder($code,$response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
             $data =  new ProductResource(Product::findOrFail($id));
             $data->delete();
             $code = 200;
            $response = $data;
            
        } catch (Exception $e) {
            $code = 500;
            $response = $e->getMessage(); 
        }
        return apiResponseBuilder($code,$response);
    }
}
