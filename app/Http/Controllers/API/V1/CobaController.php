<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CobaController extends Controller {

	// private function apiResponseBuilder($code,$data)
	// {
	// 	if ($code == 200) {
	// 		$response['status'] = 200;
	// 		$response['message'] = $data;
	// 	}else{
	// 		$response['status'] = 500;
	// 		$response['message'] = $data;
	// 	}
	// 	return response()->json($response , $code);
	// }

	public function method_get()
	{
		$data = [
			'nama' => 'diana',
			'umur' => '18',
			'sekolah' => [
				'smkt telkomm',
				'smp 2 pandaan'
			]
		];

		return apiResponseBuilder(200,$data);
		
	}

	public function method_post(Request $request)
	{

		$data = $request->all();
		if ($request->type == 'siswa') {
			$data['message'] = 'Selamat datang';
		}elseif($request->type == 'guru'){
			$data['message'] = 'Selamat datang dia';
		}else{
			$data['message'] = 'test';
		}
		return $this->apiResponseBuilder(200,$data);
	}

	public function method_try(Request $request)
	{
		try {

		$data = $request->all();
		if (($request->type != 'siswa') and ($request->type != 'guru') ) throw new \Exception("tidak ada data", 1);
		//if (!isset($request->nama)) throw new Exception("data harus isi", 1);
		$code = 200;
		$response = $data;
		//return $this->apiResponseBuilder(500, $data);
		
		} catch (Exception $e) {
			$ode = 500;
			$response = $e->getMessage(); 
           //return $this->apiResponseBuilder(500, $e->getMessage());
		}

		return $this->apiResponseBuilder($code,$response);
	}



}

/* End of file CobaController.php */
/* Location: .//D/xampp/htdocs/DOT_ACADEMY/dot_market/app/Http/Controllers/API/V1/CobaController.php */
?>