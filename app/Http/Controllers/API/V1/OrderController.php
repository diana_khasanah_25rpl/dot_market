<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customers;
use App\Orders;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;


class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       try {
        $data = $this->validate($request,[
            'customer_id' => 'required'
        ]);

        $data = new Orders();
        $data->customer_id = $request->customer_id;
        $data->total = 0;
        $data->save();

        $code = 200;
        $response = $data;
           
       } catch (Exception $e) {
            
            if ($e instanceof ValidationException) {
               $code = 400;
               $response = $e->errors();
            }else{
                $code = 500;
                $response = $e->getMessage(); 
            }
       }
       return apiResponseBuilder($code,$response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {

            $data = Orders::findOrFail($id);
            $code = 200;
            $response = $data;
            
        } catch (Exception $e) {
            
            if ($e instanceof ModelNotFoundException) {
                $code = 404;
                $response = "not found data";
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
        }
        return apiResponseBuilder($code,$response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
