<?php

namespace App\Http\Controllers\API\V1\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;


class RegisterController extends Controller
{
    //
    public function register(Request $request)
    {
    	try{
    	$this->validate($request,[
         'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],  

         ]);
            
            $data = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        
                ]);
      		$response['user'] = $data;
            $response['token'] = $data->createToken('myApp')->accessToken;

            $code = 200;
           // $response = $data; 
            
        } catch (Exception $e) {
            // $code = 500;
            // $response = $e->getMessage(); 
            
            if ($e instanceof ValidationException) {
               $code = 400;
               $response = $e->errors();
            }else{
                $code = 500;
                $response = $e->getMessage(); 
            }
            
        }
        return apiResponseBuilder($code,$response);
    	// return response()->json([
    	// 	'test' => "ok"
    	// ]);
    }
}
