<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{	
	/**
	* show login
	*/
    public function showLogin()
    {
    	if (Auth::user()) {
    		return redirect("/product");
    	}else{
    		return view("auth.login");
    	}
    	
    }
    /**
	* do login
	*/
    public function doLogin(Request $request)
    {  
    	//get user credential email dan password
    	$credential = $request->except("_token");
    	//attemp to login
    	// dd($request);
    	if (Auth::attempt($credential)) {
    		return redirect('/product');

    	}else{
    		return redirect()->back();
    	}
    }
  /**
	* logout
	*/
    public function logout()
    {
    	Auth::logout();
    	//redirect route namenya
    	return redirect()->route('login');
    	
    }
}
//path pakkai/