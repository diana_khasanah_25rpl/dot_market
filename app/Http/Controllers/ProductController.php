<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Session;
use App\Categorys;
use Exception;
class ProductController extends Controller
{
    //
    public function index()
    //bikin query baru
    //produk untu banyak action
    {

    	$datacategorys = Categorys::all(); //query kosong
    	$dataproduct  = Product::query()->orderBy('created_at', 'desc');
    	//seaarching data
    	//jika kosong dia ngak di proses
    	//strlen itu string len mengetahui isinya ada tau tidak
    	if (request()->has('search') && strlen(request()->query("search"))>=1) {
    		$dataproduct->where(
				"name", "like", "%" . request()->query("search") . "%"
			);

    		
    	}
    	//sorting data

    	//query pagination
    	$pagination = 5;
    	$dataproduct = $dataproduct->paginate($pagination);
    	//produck dihapus bisa tetep bisa dilihat
    	//$dataproduct = Product::paginate($pagination);

    	//mengheandle perpindahan page
    	$number =1;

    	if (request()->has('page') && request()->get('page') > 1) {
            $number += (request()->get('page') - 1) * $pagination;
        }

        $datacategorys = Categorys::select(["id", "name_category"])->orderBy("name_category")->get();


    	   return view('product.index', compact('dataproduct','datacategorys','number'));
    }
     public function detail($id)
    {
    	$dataproduct = Product::find($id);
    	// dd($dataproduct);
    	   return view('product.detail', compact('dataproduct'));
    }

    public function edit($id)
    {
    	$dataproduct = Product::find($id);
        //menampilka dropdown categori diupdate
        $datacategorys = Categorys::select(["id", "name_category"])->orderBy("name_category")->get();
    	   return view('product.edit', compact('dataproduct','datacategorys'));
    }
    public function store(Request $request)
    {
        $this->validate($request,[
         'name'          => 'required|string',
         'category_id'      => 'required|string',
         'unit_price'    => 'required|numeric',
         'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

         ]);
        try{

    	\DB::beginTransaction();
        Categorys::where('id',$request->category_id)->increment('product_count');
        
        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images'),$imageName);
        $dataproduct = new Product();
    	$dataproduct->name = $request->name;
    	$dataproduct->category_id = $request->category_id;
    	$dataproduct->unit_price = $request->unit_price;
        $dataproduct->image = $imageName;
    	$dataproduct->save();
        
    
        \DB::commit();
        $request->session()->flash('message','Berhasil Menambahkan');
        
              return redirect()->back();
        } catch (Exception $e) {
            report($e);
            \DB::rollBack();
           return redirect()->back();
        }
    	
     //    $dataproduct->name = $request->name;
    	// $dataproduct->category_id = $request->category_id;
    	// $dataproduct->unit_price = $request->unit_price;
    	// $dataproduct->save();

    	// if($dataproduct) {
    	// 	Session::flash('message','Berhasil menambahkan');
    	// }
    		

    	//    return redirect()->back();
    }
    public function update(Request $request, $id)
    {
         $this->validate($request,[
         'name'          => 'required|string',
         'category_id'      => 'required|string',
         'unit_price'    => 'required|numeric',
         'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

         ]);

    	$dataproduct = Product::find($id);
    	$dataproduct->name = $request->name;
    	$dataproduct->category_id = $request->category_id;
    	$dataproduct->unit_price = $request->unit_price;

        if ($request->image) 
        {             
        //mengambil nama image            
        $imageName = time().'.'.request()->image->getClientOriginalExtension();             
        // meletakkan image pada folder public/assets/images/products             
        request()->image->move(public_path('images'), $imageName);               
        $dataproduct->image = $imageName;         
        }


    	$dataproduct->save();

    	if($dataproduct) {
    		Session::flash('message','Berhasil update');
    	}
    	   return redirect()->back();
    }
    public function delete($id)
    {
    	$dataproduct = Product::withTrashed()->find($id);
    	
		if(!$dataproduct->trashed()) {
            $dataproduct->delete();
    		Session::flash('message','Berhasil menghapus');
    	}else{
            $dataproduct->forceDelete();
        }

    	  return redirect()->back()->with(['message'=> "Berhasil Hapus"]);
    }
}
