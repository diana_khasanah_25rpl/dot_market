<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Orders;
use App\Product;
use App\Customers;
use App\OrderDetails;
use Session;

class OrdersController extends Controller
{
    public function index(Request $request)
    {
    	# code...
    	$datacustomers = Customers::all();
    	// dd($datacustomers);
    	 

         $dataOrders = Orders::query();
         //filable
        if (request()->has("search") && strlen(request()->query("search")) >= 1) {
            $dataOrders->where(
                "total", "like", "%" . request()->query("search") . "%"
            );
        }

        // Query pagination
        $pagination = 5;
        $dataOrders = $dataOrders->latest()->paginate($pagination);

        // Handle perpindahan page
        $number = 1; // Default page

        if (request()->has('page') && request()->get('page') > 1) {
            $number += (request()->get('page') - 1) * $pagination;
        }

         return view('orders.index', compact('datacustomers','number' ,'dataOrders'));
    }

    public function store(Request $request)
    {
    	//validasi
        $this->validate($request,[
         'customer_id'          => 'required',
         

         ]);
    	$dataorder = new Orders;
        $dataorder->customer_id = $request->customer_id;
        $dataorder->total       = 0;
        $dataorder->save();
    	return redirect()->back()->with('success','Berhasil menambahkan');
    }

    public function detail($id)
    {
        $dataProduct = Product::all();
        $dataOrderDetail = OrderDetails::where('order_id', $id)->get();
        $total = Orders::select('total')->where('id', $id)->first();
        $detailId = $id;

        return view('orders.detail',compact('dataProduct','dataOrderDetail','detailId','total'));
    }

    public function addOrder(Request $request, $id)
    {
        $request->validate([
            'product_id' => 'required',
            'quantity' => 'required',
        ]);

        //get data order by id produk
        $checkProduct = OrderDetails::where('product_id', $request->product_id)
                                    ->where('order_id', $id)->first();
        //ambil data product by id untuk melihat data harga product
        $dataProduct = Product::where('id', $request->product_id)->first();


        //cek ada tidaknya data atau tidak
        //jika ada product dia bakal nambah quantity dan price
        if ($checkProduct) {
            $checkProduct->order_id = $checkProduct->order_id;
            $checkProduct->quantity = $checkProduct->quantity + $checkProduct->quantity;
            $checkProduct->price = $checkProduct->price +($request->quantity * $dataProduct->unit_price);
            $checkProduct->save();
        }else{//kondisi belum ada produk membuat detail order
            $dataOrderDetail = new OrderDetails;
            $dataOrderDetail->order_id = $id;
            $dataOrderDetail->product_id = $request->product_id;
            $dataOrderDetail->quantity = $request->quantity;
            $dataOrderDetail->price = $request->quantity * $dataProduct->unit_price;
            $dataOrderDetail->save();
        }

        $getOrder = OrderDetails::where('order_id',$id)->get();
        $total = $getOrder->sum('price');
        $dataorder = Orders::find($id);
        $dataorder->total = $total;
        $dataorder->save();

        return redirect()->back()->with('success', 'Berhasil Menambahkan detail dan order');    
    }

    

    public function delete($id)
    {
        $dataOrder = Orders::find($id);
        $dataOrder->delete();

        return redirect()->back()
                         ->with('success', 'Berhasil Menghapus orderan');
    }
}
