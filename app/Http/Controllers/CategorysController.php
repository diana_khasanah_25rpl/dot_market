<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categorys;
use Session;
class CategorysController extends Controller
{
    //
	public function index()
    {
    	# code...
    	// $datacategorys = Categorys::all();
    	//  return view('categorys.index', compact('datacategorys'));

    	 $datacategorys = Categorys::query();
         //filable
         //searching data
        if (request()->has("search") && strlen(request()->query("search")) >= 1) {
            $datacategorys->where(
                "name_category", "like", "%" . request()->query("search") . "%"
            );
        }

        // Query pagination
        $pagination = 5;
        $datacategorys = $datacategorys->orderBy('name_category','desc')->paginate($pagination);

        // Handle perpindahan page
        $number = 1; // Default page

        if (request()->has('page') && request()->get('page') > 1) {
            $number += (request()->get('page') - 1) * $pagination;
        }

        return view('categorys.index', compact('datacategorys', 'number'));

    }
//iki
    public function detail($id)
    {
    	$datacategorys = Categorys::with("products")->find($id);
    	// dd($dataproduct);
    	   return view('categorys.detail', compact('datacategorys'));
    }

    public function edit($id)
    {
    	$datacategorys = Categorys::find($id);

    	   return view('categorys.edit', compact('datacategorys'));
    }

    public function store(Request $request)
    {

        $this->validate($request,[
         'name_category'          => 'required',
         

         ]);

    	$datacustomers = new Categorys;
    	$datacustomers->name_category = $request->name_category;
        $datacustomers->product_count = 0;
    	

    	$datacustomers->save();

    	if($datacustomers) {
    		Session::flash('message','Berhasil Menambahkan');
    	}
    	   return redirect()->back();
    }

    public function update(Request $request, $id)
    {
    	$datacategorys = Categorys::find($id);
    	//dd($datacustomers);
    	$datacategorys->name_category = $request->name_category;
    

    	$datacategorys->save();

    	if($datacategorys) {
    		Session::flash('message','Berhasil update');
    	}
    	   return redirect()->back();
    }

     public function delete($id)
    {

        $datacategorys = Categorys::withTrashed()->find($id);
        if(!$datacategorys->trashed()) {
            $datacategorys->delete();
        }else{
            $datacategorys->forceDelete();
        }
            return redirect()->back()->with(['message'=> "Berhasil Hapus"]); 
    }
}
