<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customers;
use Session;
class CustomersController extends Controller
{
    //
    public function index()
    {
        $datacustomers = Customers::query();
         //filable
        if (request()->has("search") && strlen(request()->query("search")) >= 1) {
            $datacustomers->where(
                "first_name", "like", "%" . request()->query("search") . "%"
            )->orwhere(
                "last_name", "like", "%" . request()->query("search") . "%");
        }

        // Query pagination
        $pagination = 5;
        $datacustomers = $datacustomers->latest()->withTrashed()->paginate($pagination);

        // Handle perpindahan page
        $number = 1; // Default page

        if (request()->has('page') && request()->get('page') > 1) {
            $number += (request()->get('page') - 1) * $pagination;
        }

    	 return view('customers.index', compact('datacustomers','number'));
    }

    public function detail($id)
    {
    	$datacustomers = Customers::find($id);
    	// dd($dataproduct);
    	   return view('customers.detail', compact('datacustomers'));
    }

    public function edit($id)
    {
    	$datacustomers = Customers::find($id);

    	   return view('customers.edit', compact('datacustomers'));
    }

    public function store(Request $request)
    {

        $this->validate($request,[
         'email'          => 'required',
         'first_name'      => 'required',
         'last_name'    => 'required',
         'address' => 'required',
         'phone_number' => 'required|numeric',
         'password'=>"required",

         ]);

    	$datacustomers = new Customers;
    	$datacustomers->email = $request->email;
    	$datacustomers->first_name = $request->first_name;
    	$datacustomers->last_name = $request->last_name;
    	$datacustomers->address = $request->address;
    	$datacustomers->phone_number = $request->phone_number;
    	$datacustomers->password = $request->password;

    	$datacustomers->save();

    	if($datacustomers) {
    		Session::flash('message','Berhasil Menambahkan');
    	}
    	   return redirect()->back();
    }

    public function update(Request $request, $id)
    {
    	$datacustomers = Customers::find($id);
    	//dd($datacustomers);
    	$datacustomers->email = $request->email;
    	$datacustomers->first_name = $request->first_name;
    	$datacustomers->last_name = $request->last_name;
    	$datacustomers->address = $request->address;
    	$datacustomers->phone_number = $request->phone_number;
    	$datacustomers->password = $request->password;

    	$datacustomers->save();

    	if($datacustomers) {
    		Session::flash('message','Berhasil update');
    	}
    	   return redirect()->back();
    }

     public function delete($id)
    {
    	$datacustomers = Customers::find($id);
    	

    	$datacustomers->delete();
		if($datacustomers) {
    		Session::flash('message','Berhasil menghapus');
    	}

    	   return redirect()->back();
    }
}
