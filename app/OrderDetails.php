<?php

namespace App;
use App\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetails extends Model
{
    //
    use SoftDeletes;
     protected $table = 'order_details';
     protected $fillable =['order_id', 'product_id','quantity','price'];

     public function product()
     {
     	return $this->hasOne(Product::class, 'id',"product_id");
     }
}
