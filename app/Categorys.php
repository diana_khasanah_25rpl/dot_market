<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categorys extends Model
{
    use SoftDeletes;
    //
    protected $table = 'categorys';
    protected $fillable = ["name_category"];
    //relation to product pakai has many

    public function products()
    {
       
    	//kategiri id  sebagi foreign key dari tabel produk
    	//id  dia lokal punya kategori
    	return $this->hasMany(Product::class,'category_id','id');
    }

    public function delete()
    {
        # code...

        $this->products()->delete();
        return parent::delete();
    }
}
