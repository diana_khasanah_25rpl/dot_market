<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orders extends Model
{
	use SoftDeletes;
 
    protected $table = 'orders';

    protected $fillable = ['customer_id', 'total'];

    public function customers() {
    	return $this->hasOne(Customers::class, "id", "customer_id");
    }
    
    public function OrderDetails()
    {
    	return $this->hasMany(OrderDetails::class, "order_id","id");
    }

    public function delete()
    {
    	$this->OrderDetails()->delete();
    	return parent::delete();
    }
    
}
