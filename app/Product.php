<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Product extends Model
{
	use SoftDeletes;
    //
    protected $table = 'product';

   
     protected $fillable = ["name_category", "category_id","unit_price"];

     public function category()
     {
     	return $this->hasOne(Categorys::class,"id","category_id")->withTrashed();
     }
     //kalau kategori dihapus , product tetep bisa tampil kategori yang dihapus
}
